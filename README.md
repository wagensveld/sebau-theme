# Sebau Hugo Theme

Basic hugo theme used for [svw.au](https://svw.au) based on
[SK1 by J-Siu](https://github.com/J-Siu/hugo-theme-sk1).

## Demo

https://theme.sebb.au

## Install

Within your site's directory:

- Clone:

```
git clone https://gitlab.com/wagensveld/sebau-theme.git themes/sebau
```

- Submodule:

```
git submodule add https://gitlab.com/wagensveld/sebau-theme.git themes/sebau
```

- Update submodule:

```
git submodule update --recursive --remote
```
